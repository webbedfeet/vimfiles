xx=sims.UCI(myadult,'salary')
log.LOR <- rf.LOR <- rep(0,ncol(xx$y))
library(foreach)
library(doSNOW)
cl=makeCluster(6, type='SOCK')
registerDoSNOW(cl)
out <- foreach(i=1:ncol(xx$y),.combine=rbind,.packages=c('plyr','randomForest')) %dopar% {
    mod1 <- glm(as.factor(xx$y[,i])~., data=myadult, family=binomial)
    mod2 <- randomForest(xx$y[,i]~.,data=myadult, nodesize=10)
    P <- cbind(p=predict(mod2), myadult)
    log.LOR <- coef(mod1)["countryUSA"]
    d <- ddply(P,.(country), summarise, mean(p))[,2]
    d2 <- ddply(P, .(country), summarize, mean(log(p)-log(1-p)))[,2]
    rf.LOR <- log(d[2]*(1-d[1])/(1-d[2])/d[1])
    rf.LOR2 <- d2[2]-d2[1]
    c(log.LOR, rf.LOR,rf.LOR2)
}

