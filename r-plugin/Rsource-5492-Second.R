new.sid1.extra$assessor[grep('Weisman',new.sid1.extra$assessor)] <- 
    'Dr. Weisman'
new.sid1.extra$assessor[grep('Reveille',new.sid1.extra$assessor)] <- 
    'Dr. Reveille'
docs <- paste('Dr.',c('Davis','Gensler','Weisman','Ward','Reveille'))
new.sid1.extra <- subset(new.sid1.extra, assessor %in% docs)
