plt.haq <- ggplot(bl.haq, aes(x=variable, y=.id))+geom_tile(aes(fill=value))+
    scale_fill_gradient(low='blue',high='red',
                         breaks=c(-8,-6,-4,-2,2,4,6,8))+
    labs(y='HAQ components',x='Clinical measures', fill='z-values')+
 #   scale_x_continuous(breaks=1:14, labels=colnames(haq.zscores)[-1])+
    opts(axis.text.x=theme_text(angle=-45,hjust=0.25,vjust=0.75))
