names(ids) <- as.character(mnths)

data.imputed.bySurv <- llply(ids,
	function(x){
		data.survival.imputed[data.survival.imputed$mrno %in% x,]
	}
)
baseline.imputed.bySurv <- llply(ids,
	function(x) {
		baseline.imputed[baseline.imputed$mrno %in% x,]
	}
	)
# Need to recompute survival time based on when snapshot is taken
# Need to compute duration of pred and mtx use for each
require(reshape2)
bl <- melt(data1.imputed[,c('mrno','phase', 'pred')], id=c('mrno','phase'))
preduse <- acast(bl, phase~mrno)
lab.mrno <- colnames(preduse); lab.phase <- row.names(preduse)
preduse <- apply(preduse,1,as.numeric)-1
row.names(preduse) <- lab.mrno; colnames(preduse) <- lab.phase

# Compute duration of prednisone use prior to the survival cutoff. This is the input that is being
# used to predict survival
pred.dur <- vector('list',length(mnths))
for(i in 1:length(mnths)){
	bl <- rep(0,nrow(baseline.imputed.bySurv[[i]]))
	for(j in 1:nrow(baseline.imputed.bySurv[[i]])){
		start <- as.numeric(as.character(baseline.imputed.bySurv[[i]]$phase[j]))
		index <- start:(start+phzs[i]-1)
		bl[j] <- sum(preduse[baseline.imputed.bySurv[[i]]$mrno[j],index], na.rm=T)
	}
	pred.dur[[i]] <- data.frame(mrno=baseline.imputed.bySurv[[i]]$mrno, predDuration=bl)
	baseline.imputed.bySurv[[i]]$predDuration <- pred.dur[[i]]$predDuration
}

# Compute duration of MTX use prior to the survival cutoff. This is the input that is being
# used to predict survival
bl <- melt(data1.imputed[,c('mrno','phase','mtx'),], id=c('mrno','phase'))
mtxuse <- acast(bl, phase~mrno)
lab.mrno <- colnames(mtxuse); lab.phase <- row.names(mtxuse)
mtxuse <- apply(mtxuse, 1, as.numeric)-1
row.names(mtxuse) <- lab.mrno; colnames(mtxuse) <- lab.phase
mtx.dur <- vector('list',length(mnths))
for (i in 1:length(mnths)) {
	bl <- rep(0, nrow(baseline.imputed.bySurv[[i]]))
	for(j in 1:nrow(baseline.imputed.bySurv[[i]])){
		start <- as.numeric(as.character(baseline.imputed.bySurv[[i]]$phase[j]))
		index <- start:(start+phzs[i]-1)
		bl[j] <- sum(mtxuse[baseline.imputed.bySurv[[i]]$mrno[j],index], na.rm=T)
	}
	mtx.dur[[i]] <- data.frame(mrno=baseline.imputed.bySurv[[i]]$mrno, mtxDuration=bl)
	baseline.imputed.bySurv[[i]]$mtxDuration <- mtx.dur[[i]]$mtxDuration
}

for(i in 1:length(mnths)){
	baseline.imputed.bySurv[[i]]=transform(baseline.imputed.bySurv[[i]], newSurvtimem=Survtimem-mnths[i],
		everpred = as.factor(ifelse(predDuration==0,0,1)),
		evermtx = as.factor(ifelse(mtxDuration==0,0,1)))
}

