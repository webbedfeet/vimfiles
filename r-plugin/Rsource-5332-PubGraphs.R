p1 <- ggplot(d.dat1a, aes(value))+geom_histogram()+
        facet_wrap(~variable, nrow=1, scales='free_x')+
        xlab('')+ylab('')+opts(axis.text.x=theme_blank(),
                               axis.text.y=theme_blank())+
        opts(plot.margin=unit(c(1,1,-1.5,1.5),'lines'))
p2 <- ggplot(d.dat1, aes(d.crp))+geom_histogram()+
        opts(axis.text.y=theme_blank())+xlab('')+ylab('')+
        coord_flip()+ylim(0,600)+opts(plot.margin=unit(c(1.5,1,1,-1),'lines'))
p3 <- ggplot(d.dat1, aes(d.esr))+geom_histogram()+
        opts(axis.text.y=theme_blank())+xlab('')+ylab('')+
        coord_flip()+ylim(0,600)+opts(plot.margin=unit(c(1.5,1,1,-1),'lines'))
p4 <- ggplot(d.dat1a, aes(value, d.crp))+geom_point()+
        facet_wrap(~variable, scales='free_x', nrow=1)+
        opts(axis.text.x=theme_blank())+xlab('')+ylab('CRP')
p5 <- ggplot(d.dat1a, aes(value, d.esr))+geom_point()+
        facet_wrap(~variable, scales='free_x', nrow=1)+
        xlab('')+ylab('ESR')
p6 <- qplot(1,1,geom='blank')+scale_x_continuous('',breaks=NA)+
        scale_y_continuous('',breaks=NA)+
        opts(panel.background=theme_rect(colour=NA))
plt <- grid.arrange(p1,p6,p4,p2,p5,p3, widths=unit(c(10,2),'null'))
