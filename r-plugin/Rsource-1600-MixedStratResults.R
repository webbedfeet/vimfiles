out.f <- function(x)
{
  z <- apply(x,1,function(y){c(y[1],paste(y[2],' (',y[3],',',y[4],')',sep=''),
                          paste(y[5],' (',y[6],',',y[7],')',sep=''),
                          paste(y[8],' (',y[9],',',y[10],')',sep=''))})
  z <- data.frame(t(z))
  names(z) <- c("Predictor",'Overall','AS duration < 20 years', 
                'AS duration > 20 years')
  return(z)

}
library(xtable)
print(xtable(out.f(esr.strata)), type='HTML',file='doc/ESR_Strat.html')
print(xtable(out.f(crp.strata)),type='HTML',file='doc/CRP_Strat.html')
