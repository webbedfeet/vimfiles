plt <- ggplot(subset(subgrps, subgrp.ind=='Peripheral arthritis'), aes(y=effect, x=tvalue, groups=outcome,
                           shape=outcome))+
    geom_rect(xmin=-2, xmax=2, ymin=0, ymax=15,colour=NA, fill='grey',
              alpha=0.05)+
    geom_point()+geom_vline(xintercept=0, lty=2)+
    labs(x='T statistic', y='', shape='Measure')+
    theme_bw()
plt
